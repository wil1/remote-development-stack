#!/usr/bin/env node
const fetch = require('node-fetch')
const parseString = require('xml2js').parseString
const exec = require('child_process').exec
const fs = require('fs')

const homedir = require('os').homedir()


function log(msg) {
  console.log(msg)
}
function execShellCommand(cmd) {
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.warn(error)
      }
      resolve(stdout ? stdout : stderr)
    })
  })
}

async function parseXml(xml) {
  return new Promise(function (resolve, reject) {
    parseString(xml, function (err, result) {
      if (err) {
        reject(err)
      }
      else {
        resolve(result)
      }
    })
  })
}

async function getLatestIdeaVersion() {
  let res = await fetch('https://www.jetbrains.com/updates/updates.xml')
  let body = await res.text()

  let parsed = await parseXml(body)
  let result = null
  if (parsed && parsed.products && parsed.products.product) {
    parsed.products.product.forEach(product => {
      if (product.$.name === 'IntelliJ IDEA') {
        product.channel.forEach(channel => {
          if (channel.$.id === 'IC-IU-RELEASE-licensing-RELEASE') {
            channel.build.sort((a, b) => (a.$.releaseDate > b.$.releaseDate) ? -1 : 1)
            result = channel.build[0].$.version
          }
        })
      }
    })
  }
  return result
}

async function downloadIdea(version) {
  let fileName = `${homedir}/idea.tar.gz`
  let res = await fetch(`https://download.jetbrains.com/idea/ideaIU-${version}.tar.gz`)
  return new Promise((resolve, reject) => {
    const dest = fs.createWriteStream(fileName)
    res.body.pipe(dest)
    dest.on('close', () => resolve(fileName))
    dest.on('error', reject)
  })
}

async function run() {
  let name = 'IDEA'
  if (fs.existsSync(`${homedir}/idea`)) {
    log(`${name} is already installed`)
  } else {
    log(`Checking for latest version of ${name}`)
    let version = await getLatestIdeaVersion()
    log(`Downloading version ${version}`)
    let fileName = await downloadIdea(version)
    log(`Unpacking file`)
    await execShellCommand(`tar zxf ${fileName} -C ${homedir}`)
    await execShellCommand(`mv ${homedir}/idea-* ${homedir}/idea`)
  }
  log(`Launching ${name}`)
  await execShellCommand(`${homedir}/idea/bin/idea.sh`)
}

run()



