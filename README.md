# Remote Development Stack
This stack provides containers supporting remote development including a web IDE, Remote Desktop, single sign-on, and more.

## Prerequisites
1. Server with at least 1 public IP address AND 1 private IP address

- Public IP address is required for Nginx proxy and LetsEncrypt
- Private IP address is required for Docker swarm mode (--advertise-addr option)
- Recommend setting host kernel parameter `fs.inotify.max_user_watches=524288` for hot reload to work in containers


2. Docker, docker-compose, & swarm mode

- https://www.docker.com/get-started
- https://www.katacoda.com/courses/docker/deploying-first-container
- https://www.katacoda.com/courses/docker-orchestration/getting-started-with-swarm-mode

3. Portainer proxy stack

- https://gitlab.com/wil1/portainer-proxy-stack


## Deploying

1. Copy .env-example to .env

2. Edit .env and set `STACK_NAME` and `BASE_DOMAIN` values

3. Change the secret name at the top of docker-compose.yml (see the comment)

- e.g. if `STACK_NAME` is `mystack`, the secret name would be `mystack_admin_secret`

4. Add `${STACK_NAME}_admin_secret` secret to Docker swarm.
- `echo "MySecretPassword" | docker secret create ${STACK_NAME}_admin_password -`  
- Alternative: skip this step, run the next step, and copy/paste the command from the error message

5. `./deploy-stack.sh`

The following services should now be deployed at `https://${SUB_DOMAIN}.${BASE_DOMAIN}`, were `SUB_DOMAIN` is one of the following:
- `guac` - Guacamole remote desktop gateway
- `code` - vs code server
- `app` - proxy for port 8100 on VS Code server container, the default port for Ionic apps
- `java-app` - proxy for port 8081 on desktop container

Where possible, the admin user account names have been set to "admin", and the passwords to the secret password value set above.
