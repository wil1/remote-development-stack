#!/bin/bash

# loads .env file into the current environment
export $(cat .env | sed 's/#.*//g' | xargs)

# check if secrets exist
checkSecret() {
    SECRET=$1
    GENERATED_SECRET=`date | md5sum | head -c 32`
    [ ! "$(docker secret ls | grep ${SECRET})" ] \
    && echo "
    Secret required: 
        ${SECRET}

    To create a secret:

        echo \"MySecret\" | docker secret create ${SECRET} -

    Using a generated value:
        echo \"${GENERATED_SECRET}\" | docker secret create ${SECRET} -
    " \
    && exit 1
    
}
checkSecret ${STACK_NAME}_admin_password

# create network
[ ! "$(docker network ls | grep 'nginx_proxy')" ] \
    && docker network create -d overlay nginx_proxy

# merge docker-compose.yml and .env file (docker-compose config), and deploy it to docker swarm as a stack
# (docker-compose config) - https://github.com/moby/moby/issues/29133#issuecomment-442912378
docker stack deploy -c <(docker-compose config) $STACK_NAME
